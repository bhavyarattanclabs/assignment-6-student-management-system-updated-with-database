package com.example.rattan_bhavya.studentmanagementsystem.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rattan_bhavya.studentmanagementsystem.R;
import com.example.rattan_bhavya.studentmanagementsystem.utils.AppConstants;


public class LoginPageActivity extends Activity implements AppConstants {

    int flag = 1;

    EditText oEtUser;
    EditText oEtPass;
    Button oBtnLogin;
    Button oBtnCancel;
    TextView oTvVuser;
    TextView oTvVpass;

    SharedPreferences loginPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        oEtUser = (EditText) findViewById(R.id.etName);
        oEtPass = (EditText) findViewById(R.id.etPass);
        oBtnLogin = (Button) findViewById(R.id.btnLogin);
        oBtnCancel = (Button) findViewById(R.id.btnCancel);
        oTvVuser = (TextView) findViewById(R.id.tvVuser);
        oTvVpass = (TextView) findViewById(R.id.tvVpass);

        oBtnLogin.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (oEtUser.getText().toString().equals("")) {
                    oTvVuser.setVisibility(View.VISIBLE);
                    flag = 0;
                }
                if (oEtPass.getText().toString().equals("")) {
                    oTvVpass.setVisibility(View.VISIBLE);
                    flag = 0;
                }

                flag = 1;

                if (flag != 0) {
                    String username = oEtUser.getText().toString();
                    String password = oEtPass.getText().toString();

                    loginPreference = getSharedPreferences(SHARED_PREFERENCES_FILENAME, PREFRERENCE_FILE_MODE);

                    String name = loginPreference.getString("name", "Login for first time");
                    String pass = loginPreference.getString("pass", "Login for first time");

                    if (name.equals("Login for first time") && pass.equals("Login for first time")) {
                        SharedPreferences.Editor editor = loginPreference.edit();
                        editor.putString("name", username);
                        editor.putString("pass", password);
                        editor.commit();
                    } else {

                        if (username.equals(name)) {
                            if (password.equals(pass))

                                Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(LoginPageActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else
                            Toast.makeText(getApplicationContext(), "Invalid username or password", Toast.LENGTH_LONG).show();

                    }
                }

            }
        });

        oEtUser.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                oTvVuser.setVisibility(View.INVISIBLE);
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub


            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        oEtPass.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                oTvVpass.setVisibility(View.INVISIBLE);
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        oBtnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                finish();
                //System.exit(0);
            }
        });

    }
}

