package com.example.rattan_bhavya.studentmanagementsystem.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.rattan_bhavya.studentmanagementsystem.R;
import com.example.rattan_bhavya.studentmanagementsystem.entities.Student;


public class ViewStudentDetailsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_student_details);

        Student student = (Student) getIntent().getSerializableExtra("student");
        TextView oTvName = (TextView) findViewById(R.id.tvViewName);
        TextView oTvRollno = (TextView) findViewById(R.id.tvViewRollno);
        TextView oTvAddress = (TextView) findViewById(R.id.tvViewaddress);
        TextView oTvCourse = (TextView) findViewById(R.id.tvViewCourse);
        TextView oTvBranch = (TextView) findViewById(R.id.tvViewBranch);
        TextView oTvContact = (TextView) findViewById(R.id.tvViewContact);
        TextView oTvEmailId = (TextView) findViewById(R.id.tvViewEmailid);


        oTvName.setText(student.name);
        oTvRollno.setText(Integer.toString(student.rollno));
        oTvAddress.setText(student.address);
        oTvCourse.setText(student.course);
        oTvBranch.setText(student.branch);
        oTvContact.setText(student.contact);
        oTvEmailId.setText(student.emailId);
    }

    public void Back(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                finish();
                break;
            default:
                break;

        }
    }
}
