package com.example.rattan_bhavya.studentmanagementsystem.entities;

import java.io.Serializable;

/**
 * Created by RATTAN-BHAVYA on 1/25/2015.
 */
public class Student implements Serializable {
    public String name;
    public int rollno;
    public String address;
    public String course;
    public String branch;
    public String contact;
    public String emailId;


    public Student(String name, int rollno, String address, String course, String branch, String contact, String emailId) {
        this.name = name;
        this.rollno = rollno;
        this.address = address;
        this.course = course;
        this.branch = branch;
        this.contact = contact;
        this.emailId = emailId;
    }

}
