package com.example.rattan_bhavya.studentmanagementsystem.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rattan_bhavya.studentmanagementsystem.R;
import com.example.rattan_bhavya.studentmanagementsystem.entities.Student;
import com.example.rattan_bhavya.studentmanagementsystem.utils.AppConstants;


public class EditStudentDetailsActivity extends Activity implements AppConstants {

    EditText oEtName;
    EditText oEtAddress;
    EditText oEtCourse;
    EditText oEtBranch;
    EditText oEtContact;
    EditText oEtEmailId;

    Student student;
    String name;
    int rollno;
    String address;
    String course;
    String branch;
    String contact;
    String emailId;


    int position;
    int flag = VALID_DATA;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student_details);

        student = (Student) getIntent().getSerializableExtra("student");

        position = getIntent().getIntExtra("position", -1);

        oEtName = (EditText) findViewById(R.id.etEditName);
        oEtAddress = (EditText) findViewById(R.id.eteditaddress);
        oEtCourse = (EditText) findViewById(R.id.eteditcourse);
        oEtBranch = (EditText) findViewById(R.id.eteditbranch);
        oEtContact = (EditText) findViewById(R.id.eteditcontact);
        oEtEmailId = (EditText) findViewById(R.id.eteditemailid);


        oEtName.setText(student.name);
        oEtAddress.setText(student.address);
        oEtCourse.setText(student.course);
        oEtBranch.setText(student.branch);
        oEtContact.setText(student.contact);
        oEtEmailId.setText(student.emailId);

    }

    public void editStudent(View view) {
        switch (view.getId()) {

            case R.id.btnSaveEdit:
                intent = new Intent();
                if (oEtName.getText().toString().trim().equals("") || oEtAddress.getText().toString().trim().equals("") || oEtCourse.getText().toString().trim().equals("") || oEtBranch.getText().toString().trim().equals("") || oEtContact.getText().toString().trim().equals("") || oEtEmailId.getText().toString().trim().equals("")) {

                    AlertDialog alertDialog = new AlertDialog.Builder(
                            EditStudentDetailsActivity.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("Warning");
                    // Setting Dialog Message
                    alertDialog.setMessage("Please fill all the boxes\n");
                    // Showing Alert Message
                    alertDialog.show();
                    flag = INVALID_DATA;
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches()) {
                    AlertDialog alertDialog = new AlertDialog.Builder(
                            EditStudentDetailsActivity.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("Warning");
                    // Setting Dialog Message
                    alertDialog.setMessage("Enter a valid email id\n");
                    // Showing Alert Message
                    alertDialog.show();
                    flag = INVALID_DATA;
                } else {
                    flag = VALID_DATA;
                }
                if (flag != INVALID_DATA) {

                    name = oEtName.getText().toString();
                    rollno = student.rollno;
                    address = oEtAddress.getText().toString();
                    course = oEtCourse.getText().toString();
                    branch = oEtBranch.getText().toString();
                    contact = oEtContact.getText().toString();
                    emailId = oEtEmailId.getText().toString();

                    student = new Student(name, rollno, address, course, branch, contact, emailId);
                    intent.putExtra("student", student);
                    intent.putExtra("position", position);
                    setResult(RESULT_OK, intent);
                    finish();
                }

                break;

            case R.id.btnCancelEdit:
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
            default:
                break;

        }
    }

}
