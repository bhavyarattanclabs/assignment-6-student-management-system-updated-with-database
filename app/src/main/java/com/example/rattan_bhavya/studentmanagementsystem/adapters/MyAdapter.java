package com.example.rattan_bhavya.studentmanagementsystem.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rattan_bhavya.studentmanagementsystem.R;
import com.example.rattan_bhavya.studentmanagementsystem.entities.Student;

import java.util.List;

/**
 * Created by RATTAN-BHAVYA on 1/26/2015.
 */
public class MyAdapter extends BaseAdapter {

    public List<Student> studentList;
    Context ctx;

    public MyAdapter(Context ctx, List<Student> studentList) {
        this.ctx = ctx;
        this.studentList = studentList;
    }

    @Override
    public int getCount() {
        return studentList.size();
    }

    @Override
    public Object getItem(int position) {
        return studentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.student_view_item, (ViewGroup) convertView);

        TextView otvname = (TextView) view.findViewById(R.id.tvname);
        TextView otvrollno = (TextView) view.findViewById(R.id.tvrollno);

        otvname.setText(studentList.get(position).name);
        otvrollno.setText(String.valueOf(studentList.get(position).rollno));
        return view;
    }
}
