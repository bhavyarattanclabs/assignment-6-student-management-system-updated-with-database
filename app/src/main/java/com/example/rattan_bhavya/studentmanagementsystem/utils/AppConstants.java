package com.example.rattan_bhavya.studentmanagementsystem.utils;

/**
 * Created by RATTAN-BHAVYA on 1/28/2015.
 */
public interface AppConstants {

    int REQUEST_CODE_ADD_STUDENT = 100;
    int REQUEST_CODE_EDIT_STUDENT = 200;
    int SORT_BY_NAME = 1;
    int SORT_BY_ROLLNO = 2;
    int VALID_DATA = 1;
    int INVALID_DATA = 0;

    String SHARED_PREFERENCES_FILENAME = "sharedPreferenceFile";
    int PREFRERENCE_FILE_MODE = 0;
}
