package com.example.rattan_bhavya.studentmanagementsystem.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rattan_bhavya.studentmanagementsystem.R;
import com.example.rattan_bhavya.studentmanagementsystem.entities.Student;
import com.example.rattan_bhavya.studentmanagementsystem.utils.AppConstants;


/**
 * Created by RATTAN-BHAVYA on 1/26/2015.
 */
public class AddStudentActivity extends Activity implements AppConstants {

    EditText oEtName;
    EditText oEtAddress;
    EditText oEtCourse;
    EditText oEtBranch;
    EditText oEtContact;
    EditText oEtEmailId;

    Student student;
    String name;
    int rollno = 0;
    String address;
    String course;
    String branch;
    String contact;
    String emailId;


    Intent intent;
    int flag = VALID_DATA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_student);

        oEtName = (EditText) findViewById(R.id.etname);
        oEtAddress = (EditText) findViewById(R.id.etaddress);
        oEtCourse = (EditText) findViewById(R.id.etcourse);
        oEtBranch = (EditText) findViewById(R.id.etbranch);
        oEtContact = (EditText) findViewById(R.id.etcontact);
        oEtEmailId = (EditText) findViewById(R.id.etemailid);


    }

    public void addStudent(View view) {
        switch (view.getId()) {

            case R.id.btnsave:
                name = oEtName.getText().toString();
                address = oEtAddress.getText().toString();
                course = oEtCourse.getText().toString();
                branch = oEtBranch.getText().toString();
                contact = oEtContact.getText().toString();
                emailId = oEtEmailId.getText().toString();


                intent = new Intent();
                if (name.trim().equals("") || address.trim().equals("") || course.trim().equals("") || branch.trim().equals("") || contact.trim().equals("") || emailId.trim().equals("")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(
                            AddStudentActivity.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("Warning");
                    // Setting Dialog Message
                    alertDialog.setMessage("Please fill all the boxes\n");
                    // Showing Alert Message
                    alertDialog.show();
                    flag = INVALID_DATA;
                } else if (name.startsWith(" ") || address.startsWith(" ") || course.startsWith(" ") || branch.startsWith(" ") || contact.startsWith(" ") || emailId.startsWith(" ")) {
                    Toast.makeText(this, "A field can't start with a space at front", Toast.LENGTH_LONG).show();
                    flag = INVALID_DATA;
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches()) {
                    AlertDialog alertDialog = new AlertDialog.Builder(
                            AddStudentActivity.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("Warning");
                    // Setting Dialog Message
                    alertDialog.setMessage("Enter a valid email id\n");
                    // Showing Alert Message
                    alertDialog.show();
                    flag = INVALID_DATA;
                } else {
                    flag = VALID_DATA;
                }


                if (flag != INVALID_DATA) {

                    student = new Student(name, rollno, address, course, branch, contact, emailId);
                    intent.putExtra("student", student);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;

            case R.id.btncancel:
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
            default:
                break;

        }
    }
}