package com.example.rattan_bhavya.studentmanagementsystem.utils;

/**
 * Created by RATTAN-BHAVYA on 2/4/2015.
 */
public interface DbConstants {

    String DATABASE_NAME = "StudentManagementSystemDb";
    String TABLE_NAME = "StudentRecordTable";
    int DATABASE_VERSION = 1;

    String NAME = "student_name";
    String ROLL_NO = "student_roll_no";
    String ADDRESS = "student_address";
    String COURSE = "course";
    String BRANCH = "branch";
    String CONTACT = "contact";
    String EMAIL_ID = "email_id";

}
