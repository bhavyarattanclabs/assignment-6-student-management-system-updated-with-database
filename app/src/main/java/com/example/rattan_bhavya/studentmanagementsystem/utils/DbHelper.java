package com.example.rattan_bhavya.studentmanagementsystem.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by RATTAN-BHAVYA on 2/4/2015.
 */
public class DbHelper extends SQLiteOpenHelper implements DbConstants {

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                        ROLL_NO + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        NAME + " TEXT NOT NULL, " +
                        ADDRESS + " TEXT NOT NULL, " +
                        COURSE + " TEXT NOT NULL, " +
                        BRANCH + " TEXT NOT NULL, " +
                        CONTACT + " TEXT NOT NULL, " +
                        EMAIL_ID + " TEXT NOT NULL);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
