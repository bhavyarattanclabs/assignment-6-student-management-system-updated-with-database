package com.example.rattan_bhavya.studentmanagementsystem.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.rattan_bhavya.studentmanagementsystem.entities.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RATTAN-BHAVYA on 2/4/2015.
 */
public class DbController implements DbConstants {

    private SQLiteDatabase db;
    private DbHelper dbHelper;
    private Context context;

    public DbController open() {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();

        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public DbController(Context context) {
        this.context = context;
    }

    public int addRecord(Student student) {
        ContentValues cv = new ContentValues();
        cv.put(NAME, student.name);
        cv.put(ADDRESS, student.address);
        cv.put(COURSE, student.course);
        cv.put(BRANCH, student.branch);
        cv.put(CONTACT, student.contact);
        cv.put(EMAIL_ID, student.emailId);
        return (int) db.insert(TABLE_NAME, null, cv);

    }


    public List getAllRecords() {
        String[] columns = new String[]{NAME, ADDRESS, COURSE, BRANCH, CONTACT, EMAIL_ID};
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);

        List<Student> addStudent = new ArrayList<Student>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String name = cursor.getString(cursor.getColumnIndex("student_name"));
                int rollno = cursor.getInt(cursor.getColumnIndex("student_roll_no"));
                String address = cursor.getString(cursor.getColumnIndex("student_address"));
                String course = cursor.getString(cursor.getColumnIndex("course"));
                String branch = cursor.getString(cursor.getColumnIndex("branch"));
                String contact = cursor.getString(cursor.getColumnIndex("contact"));
                String emailId = cursor.getString(cursor.getColumnIndex("email_id"));

                addStudent.add(new Student(name, rollno, address, course, branch, contact, emailId));
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        return addStudent;
    }

    public void updateRecord(Student student) {
        ContentValues cvUpdate = new ContentValues();
        cvUpdate.put(NAME, student.name);
        cvUpdate.put(ADDRESS, student.address);
        cvUpdate.put(COURSE, student.course);
        cvUpdate.put(BRANCH, student.branch);
        cvUpdate.put(CONTACT, student.contact);
        cvUpdate.put(EMAIL_ID, student.emailId);

        db.update(TABLE_NAME, cvUpdate, ROLL_NO + "=" + student.rollno, null);
    }

    public void deleteRecord(int rollno) {
        db.delete(TABLE_NAME, ROLL_NO + "=" + rollno, null);
    }
}

