package com.example.rattan_bhavya.studentmanagementsystem.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.rattan_bhavya.studentmanagementsystem.entities.Student;
import com.example.rattan_bhavya.studentmanagementsystem.utils.AppConstants;
import com.example.rattan_bhavya.studentmanagementsystem.utils.DbController;
import com.example.rattan_bhavya.studentmanagementsystem.adapters.MyAdapter;
import com.example.rattan_bhavya.studentmanagementsystem.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class MainActivity extends Activity implements AppConstants {

    ListView studentListView;
    MyAdapter adapter;
    List<Student> studentList;
    GridView studentGridView;
    static Spinner spinner;
    int position;
    ProgressDialog progress;

    final String[] spinnerSort = {"Choose Option", "Name", "Roll No."};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (Spinner) findViewById(R.id.spinner_sort);

        new ProcessingViewRecords().execute();

        studentListView = (ListView) findViewById(R.id.studentListView);
        studentGridView = (GridView) findViewById(R.id.studentGridView);

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddStudent:
                Intent intentAddStudent = new Intent(MainActivity.this, AddStudentActivity.class);
                startActivityForResult(intentAddStudent, REQUEST_CODE_ADD_STUDENT);
                break;
            case R.id.btnListView:
                studentListView.setVisibility(View.VISIBLE);
                studentGridView.setVisibility(View.INVISIBLE);
                break;
            case R.id.btnGridView:
                studentGridView.setVisibility(View.VISIBLE);
                studentListView.setVisibility(View.INVISIBLE);
                break;
            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_ADD_STUDENT) {
            if (resultCode == RESULT_OK) {

                Student student = (Student) data.getSerializableExtra("student");
                new ProcessingAdd().execute(student);

            } else if (resultCode == RESULT_CANCELED) {

            }
        }

        if (requestCode == REQUEST_CODE_EDIT_STUDENT) {
            if (resultCode == RESULT_OK) {

                Student student = (Student) data.getSerializableExtra("student");
                position = data.getIntExtra("position", -1);

                new ProcessingEdit().execute(student);

            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }

    public void itemClicked(AdapterView<?> parent, View view, final int position, long id) {
        final Dialog dialogBox = new Dialog(MainActivity.this);
        dialogBox.setContentView(R.layout.dialog_on_item);
        dialogBox.setTitle("Choose an option");
        dialogBox.show();

        Button obtnView = (Button) dialogBox.findViewById(R.id.btnView);
        obtnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentViewStudent = new Intent(MainActivity.this, ViewStudentDetailsActivity.class);
                intentViewStudent.putExtra("student", studentList.get(position));

                startActivity(intentViewStudent);
                dialogBox.dismiss();
            }
        });

        Button oBtnEdit = (Button) dialogBox.findViewById(R.id.btnEdit);
        oBtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentEditStudent = new Intent(MainActivity.this, EditStudentDetailsActivity.class);

                intentEditStudent.putExtra("student", studentList.get(position));
                intentEditStudent.putExtra("position", position);
                startActivityForResult(intentEditStudent, REQUEST_CODE_EDIT_STUDENT);
                dialogBox.dismiss();
            }
        });

        Button oBtnDelete = (Button) dialogBox.findViewById(R.id.btnDelete);
        oBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Student student = studentList.get(position);
                new ProcessingDelete().execute(student);
                dialogBox.dismiss();
            }
        });
    }

    protected void myProgressBar() {
        progress = new ProgressDialog(MainActivity.this);
        progress.setMessage("Fetching records. Please wait...");
        progress.setIndeterminate(false);
        progress.setMax(100);
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setCancelable(false);
        progress.show();
    }

    public class ProcessingViewRecords extends AsyncTask<List<Student>, Integer, List<Student>> {

        @Override
        protected void onPreExecute() {

            myProgressBar();
        }

        @Override
        protected List<Student> doInBackground(List<Student>... params) {

            DbController dbViewRecords = new DbController(MainActivity.this);
            dbViewRecords.open();
            studentList = new ArrayList<Student>(dbViewRecords.getAllRecords());
            dbViewRecords.close();

            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
            }

            return studentList;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progress.setProgress(values[0]);
        }


        @Override
        protected void onPostExecute(List<Student> students) {

            progress.dismiss();
            adapter = new MyAdapter(MainActivity.this, students);

            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(MainActivity.this, R.layout.my_spinner_item, spinnerSort);
            spinner.setAdapter(spinnerAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == SORT_BY_NAME) {
                        Collections.sort(studentList, new Comparator<Student>() {
                            @Override
                            public int compare(Student s1, Student s2) {
                                return s1.name.toUpperCase().compareTo(s2.name.toUpperCase());
                            }
                        });
                    }
                    if (position == SORT_BY_ROLLNO) {
                        Collections.sort(studentList, new Comparator<Student>() {
                            @Override
                            public int compare(Student s1, Student s2) {
                                return s1.rollno - s2.rollno;
                            }
                        });
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Collections.sort(studentList, new Comparator<Student>() {
                        @Override
                        public int compare(Student s1, Student s2) {
                            return s1.name.toUpperCase().compareTo(s2.name.toUpperCase());
                        }
                    });
                    adapter.notifyDataSetChanged();
                }
            });
            studentListView.setAdapter(adapter);
            studentGridView.setAdapter(adapter);

            studentGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    itemClicked(parent, view, position, id);
                }
            });

            studentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    itemClicked(parent, view, position, id);

                }
            });
        }

    }

    public class ProcessingEdit extends AsyncTask<Student, Integer, Student> {


        @Override
        protected void onPreExecute() {

            myProgressBar();
        }

        @Override
        protected Student doInBackground(Student... student) {

            DbController dbUpdateOperation = new DbController(MainActivity.this);
            dbUpdateOperation.open();
            dbUpdateOperation.updateRecord(student[0]);
            dbUpdateOperation.close();

            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(6);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
            }

            return student[0];


        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            progress.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Student student) {

            progress.dismiss();
            adapter.studentList.set(position, student);
            adapter.notifyDataSetChanged();
        }
    }

    public class ProcessingAdd extends AsyncTask<Student, Integer, Student> {

        int rollno;

        @Override
        protected void onPreExecute() {

            myProgressBar();
        }

        @Override
        protected Student doInBackground(Student... student) {

            DbController dbAddOperation = new DbController(MainActivity.this);
            dbAddOperation.open();
            rollno = dbAddOperation.addRecord(student[0]);
            dbAddOperation.close();

            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(6);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
            }

            return student[0];
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            progress.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Student student) {

            progress.dismiss();
            student.rollno = rollno;
            adapter.studentList.add(student);
            adapter.notifyDataSetChanged();

        }
    }

    public class ProcessingDelete extends AsyncTask<Student, Integer, Student> {


        @Override
        protected void onPreExecute() {

            myProgressBar();
        }

        @Override
        protected Student doInBackground(Student... student) {
            DbController dbDeleteOperation = new DbController(MainActivity.this);
            dbDeleteOperation.open();
            dbDeleteOperation.deleteRecord(student[0].rollno);
            dbDeleteOperation.close();

            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(6);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
            }

            return student[0];
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            progress.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Student student) {

            progress.dismiss();
            adapter.studentList.remove(position);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        spinner.setSelection(0);
    }
}

